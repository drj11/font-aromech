# Aromech / Aromelt

Aromech is a font traced from a photo of my quick alphabet
drawn 20mm high in the style of Roman Rustic Capitals.

Aromelt is a cleanup of that.

A prominent feature of the font is the strong foot/serif,
called a _tab_.
The canonical tab is /_tab.horizontal .
The tab is 85 units high, and a slope on its verticals on 18°.

The verticals of Aromelt are
leaning to the right very slightly at 3.5°.

# END
